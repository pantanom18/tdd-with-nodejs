BRIEF
    TDD con mocha y assert. 
    Tambien, se puede usar chai en lugar de assert:
        npm install chai

        const assert = require('chai').assert;
        foo.should.be.a(value);

        const expect = require('chai').expect;
        expect(foo).to.be.a(value);

        const should = require('chai').should;
        assert.equal(foo,value);

Configuracion de test
    default
        "test": "echo \"Error: no test specified\" && exit 1"

    mocha(npm install mocha)
        "test": "./node_modules/.bin/mocha --reporter spec"
