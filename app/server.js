const express = require("express");
const converter = require('../app/converter');
//const bodyParser = require("body-parser");
const app = express();
const PORT = process.env.PORT || 3000;

//app.use(bodyParser.json());

app.get("/rbgToHex", (req,res) => {
    let red = parseInt(req.query.red,10);
    let green = parseInt(req.query.green,10);
    let blue =parseInt( req.query.blue,10);

    let hex = converter.rgbToHex(red,green,blue);

    res.status(200);
    res.send(hex);
});

app.get("/hexToRgb", (req,res) => {

    let hex = req.query.hex;
    let rgb = converter.hexToRgb(hex);
    res.status(200);
    res.send(JSON.stringify(rgb));
});

app.listen(PORT,(error) => {
    if(error)
        console.log(error);
    else
        console.log("listen ", PORT);
})
