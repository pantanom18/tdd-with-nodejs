const assert = require('assert');
const request = require('request');

describe("Color Code Converter API", () => {

  describe("RBG to Hex conversion", () => {

    var url = "http://localhost:3000/rbgToHex?red=255&green=255&blue=255";

    it("returns status 200", (done) => {

      request(url, (error, response, body) => {

        assert.equal(response.statusCode, 200);
        done();
        
      });
    });

    it("returns the color in hex", (done) => {

      request(url, (error, response, body) => {

        assert.equal(body, "ffffff");
        done();
        
      });
    });
  });

  describe("Hex to RGB conversion", () => {

    var url = "http://localhost:3000/hexToRgb?hex=00ff00";

    it("returns status 200", (done) => {

      request(url, (error, response, body) => {

        assert.equal(response.statusCode,200);
        done();
      });
    });

    it("returns the color in hex", (done) => {

        request(url,(error,response,body)=>{

          assert.equal(body,"[0,255,0]");
          done();
        });
    });
  });
}); 